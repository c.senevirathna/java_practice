GitHub is a developer platform that allows developers to create, store, manage and share their code. It uses Git software, providing the distributed version control of Git plus access control, bug tracking, software feature requests, task management, continuous integration, and wikis for every project. Wikipedia
Founders: Chris Wanstrath, Scott Chacon, Tom Preston-Werner, P. J. Hyett
Headquarters: San Francisco, California, United States
CEO: Thomas Dohmke (Nov 15, 2021–)
Founded: 2008, San Francisco, California, United States
Subsidiary: npm, Inc.
Employees: 5,595
Parent: Microsoft
