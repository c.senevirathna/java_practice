package interfaces.java;

public class Main {
    public static void main(String[] args) {
        PasswordChangeEvent passwordChangeEvent = new PasswordChangeEvent("123456789");
        MissedPaymentEvent missedPaymentEvent = new MissedPaymentEvent("9874563210");

        Event[] event = {passwordChangeEvent,missedPaymentEvent};

        for (Event e: event){
            System.out.println(e.getTimeStamp());
            e.process();
            System.out.println();

        }
    }
}
