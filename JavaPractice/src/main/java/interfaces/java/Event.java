package interfaces.java;

public interface Event {
    Long getTimeStamp();
    void process();
}
