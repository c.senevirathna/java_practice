package abtraction.java;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        DigitsOnlyFileReader digitsOnlyFileReader = new DigitsOnlyFileReader("C://Users//User//OneDrive//Documents//JavaPractice//java_practice//JavaPractice//src//main//resources//message.txt");
        System.out.println(digitsOnlyFileReader.readFile());
        System.out.println(digitsOnlyFileReader.getFilePath());
    }
}
