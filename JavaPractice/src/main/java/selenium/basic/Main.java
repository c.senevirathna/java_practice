package selenium.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.lang.model.element.Element;

public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\User\\Documents\\JavaPractice\\java_practice\\JavaPractice\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.navigate().to("https://en.wikipedia.org/wiki/Main_Page");
        driver.findElement(By.xpath("//input[@name='search']")).sendKeys("Sri Lanka");
        driver.findElement(By.xpath("//button[text() = 'Search']")).click();
        driver.quit();
    }
}
