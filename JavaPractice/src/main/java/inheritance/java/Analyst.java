package inheritance.java;

public class Analyst extends Employee{

    public  Analyst(String name, double salary, int age){
        super(name, salary, age);
    }
    public double annualBonus(){
        return super.getSalary() * 0.05;
    }


}
