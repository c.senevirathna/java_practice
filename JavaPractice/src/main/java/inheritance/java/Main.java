package inheritance.java;

public class Main {
    public static void main(String[] args) {
        Analyst c = new Analyst("Anna", 520.30, 25);
        Salesperson v = new Salesperson("Dion", 526725.26, 29, 5.3);
        System.out.println("this is analyst class");
        c.getName();
        c.raiseSalary();
        System.out.println("Analyst:"+ c.getSalary());
        System.out.println("This is Salesperson class");
        System.out.println(v.getCommissionPercentage());
    }
}
