package fundamentals.java;

import java.util.Scanner;

public class WhileLoop {

    public void WhileLoop(){

        Scanner input = new Scanner(System.in);
        boolean isOnRepeat = true;
        while (isOnRepeat){
            System.out.println("Playing current song");
            System.out.println("Do you want to NOT TO continue current song? If Yes, answer yes.");
            String userInput = input.next();

            if (userInput.equals("yes")){
                isOnRepeat = false;
            }
        }
        System.out.println("Playing Next song....");
    }
}
