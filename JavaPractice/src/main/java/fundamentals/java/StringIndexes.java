package fundamentals.java;

public class StringIndexes {

    public static void main(String[] args) {

        String firstName = "Chathurya";
        char firstLetter = firstName.charAt(0);
        System.out.println(firstLetter);

        UserInput ui = new UserInput();
        ui.UsrInput();

        WhileLoop wl = new WhileLoop();
        wl.WhileLoop();

        Constructor con1 = new Constructor(15, 8,1.5,2,3);
        double findAr = con1.findArea();
        System.out.println("Area:" + findAr);
    }
}
