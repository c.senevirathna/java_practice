package polymorphism.java;

public class Animal {
    private String sound = "Animals sound";

    public void animalSound(){
        System.out.println("Animal make sounds");
    }
    public void eats(){
        System.out.println("Animal eats");
    }
}
