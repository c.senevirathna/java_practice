package polymorphism.java;

public class Cat extends Animal{

    //method overriding
    public void animalSound(){
        System.out.println("Cat makes meow meow");
    }

    //method overloading
    public void eats(String type){
        System.out.println("Cat eats fish");
    }
}
