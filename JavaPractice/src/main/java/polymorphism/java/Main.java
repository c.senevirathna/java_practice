package polymorphism.java;

public class Main {
    public static void main(String[] args) {
        Cat c = new Cat();
        Dog d = new Dog();
        Animal a = new Animal();
        System.out.println("Cat");
        c.animalSound();
        c.eats("Cat");
        c.eats();
        System.out.println("Dog");
        d.animalSound();
        d.eats(3);
        d.eats();
        System.out.println("Animal");
        a.animalSound();
        a.eats();
    }
}
