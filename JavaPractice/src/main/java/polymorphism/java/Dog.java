package polymorphism.java;

public class Dog extends Animal {
    //method overriding
    public void animalSound(){
        System.out.println("Dog makes wuh wuh");
    }

    //method overloading
    public void eats(int i){
        System.out.println("Dog eats rice"+"Number:"+i);
    }
}
