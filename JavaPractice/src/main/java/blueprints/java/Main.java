package blueprints.java;

public class Main {

    public static void main(String[] args) {
        Tree myFavTree = new Tree(120,5, TreeType.OAK);
        myFavTree.WhenTreeTall();
        System.out.println(Tree.TRUNK_COLOR);
        Tree.announceTree();

        Employee emp1 = new Employee("Roberts", 52, 100500, "London");
        Employee emp2 = new Employee("Christen", 30, 85500, "Colombo");
        emp2.raiseSalary();
        System.out.println(emp1.getName() +"Salary:"+emp1.getSalary());
        System.out.println(emp2.getName() +"Salary:"+emp2.getSalary());
    }
}
