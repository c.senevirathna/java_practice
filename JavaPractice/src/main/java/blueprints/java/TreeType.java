package blueprints.java;

public enum TreeType {
    OAK,
    MAPLE,
    APPLE,
    WALNUT,
    PINE
}
