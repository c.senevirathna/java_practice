package blueprints.java;

import java.awt.*;

public class Tree {
    private double heightft;
    private double trunkDiameterInches;
    private TreeType treeType;
    static  Color TRUNK_COLOR = new Color(102,51,0);

    public Tree(double heightft, double trunkDiameterInches, TreeType treeType){
        this.heightft = heightft;
        this.trunkDiameterInches = trunkDiameterInches;
        this.treeType = treeType;
    }
    public void Grow(){
        this.heightft = this.heightft + 10;
        this.trunkDiameterInches = this.trunkDiameterInches + 1;
    }

    public static void announceTree(){
        System.out.println("Look out for that"+ TRUNK_COLOR+"tree!!!");
    }
    public void WhenTreeTall(){
        if(this.heightft > 100){
            System.out.println("This tree "+this.heightft+ "is tall!!");
        }
    }
}
