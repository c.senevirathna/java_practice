package encapsulation.java;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }
    public String getOwner() {
        return this.owner;
    }

    public double getBalance() {
        return this.balance;
    }

    public void withdrawMoney(double withdrawAmount){
        this.balance = this.balance - withdrawAmount;
    }

    public void depositMoney(double depositAmount){
        this.balance = this.balance - depositAmount;
    }
}
