package encapsulation.java;

public class Main {

    public static void main(String[] args) {

        //Parent p = new Parent();
        Child1 c = new Child1();
        System.out.println(c.getLname());
        c.setAge(25);
        System.out.println("Age:"+ c.getAge());

        BankAccount Acc1 = new BankAccount("Chathurya", 4500.23);
        Acc1.withdrawMoney(200);
        System.out.println(Acc1.getOwner()+"'s Bank Balance is "+Acc1.getBalance());
    }
}
