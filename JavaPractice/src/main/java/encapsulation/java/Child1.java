package encapsulation.java;

public class Child1 {

    private String fname, lname;
    private int age;
    public Child1(){
        fname = "chahtuni";
        lname = "Sene";
        age = 26;
    }

    public String getFname(){
        return fname;
    }

    public void setFname(String fname1){
        this.fname = fname1;
    }

    public String getLname() {
        return lname;
    }

    public int getAge() {
        return age;
    }

    public void setLname(String lname1) {
        this.lname = lname1;
    }

    public void setAge(int age1) {
        this.age = age1;
    }
}
