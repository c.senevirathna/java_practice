package exercise.java;

import java.sql.Array;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // option1
        System.out.println("1st way");
        ArrList cars = new ArrList();
        cars.addValue("A");
        cars.addValue("B");
        cars.addValue("C");
        cars.addValue("D");
        cars.reversed();

        // option2
        System.out.println("2nd way");
        String arr[] = new String[3];
        arr[0] = "A";
        arr[1] = "B";
        arr[2] = "C";
        for(int i = arr.length -1; 0 <= i; i--){
            System.out.println(arr[i]);
        }

        ex2 example2 = new ex2();
        example2.oddOrEven();

        SalaryCalc sal = new SalaryCalc();
        sal.SalaryCalc(20,300);

        int number;
        Scanner sc = new Scanner(System.in);
        PrimeNumber cpn = new PrimeNumber();
        StartPattern stars = new StartPattern();
        ReverseNumber reverseNum = new ReverseNumber();
        IndividualNumber individualNumber = new IndividualNumber();
        ProductOfOddNums productOfOddNums = new ProductOfOddNums();

        System.out.println("Enter an integer number:");
        number = sc.nextInt();
        cpn.checkPrimeNumber(number);
        stars.printStars();
        reverseNum.reversedNumber();
        individualNumber.printNumber();
        productOfOddNums.findProduct();

    }

}