package exercise.java;

public class PrimeNumber {

    public void checkPrimeNumber(int number){
        int divisibility = 0;

        if(number == 0 || number == 1 || number < 0){
            System.out.println(number + "is not a Prime Number.");
        }
        else{
        for (int n=number; n<=number; n--){
            if(1>=n){
                break;
            }else{
            if (number % n ==0){
            divisibility = divisibility + 1;
            }
            }
        }
        if (divisibility ==1){
            System.out.println(number + "is a Prime Number.");
        }else {
            System.out.println(number + "is a not Prime Number.");
        }

        }
    }
}
