package exercise.java.Assignment5.fruits;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Fruits {

    private final FileHandler fileHandler = new FileHandler();
    public String readWords(){

        this.fileHandler.setFilePath("E:\\Automation\\javaPractice\\java_practice\\JavaPractice\\src\\main\\resources\\input\\Assignment 5 Fruits.txt");
        return this.fileHandler.readFile();
    }

    public void writeWords(ArrayList<String> writeList, String filaName) throws IOException {
        this.fileHandler.setFilePath("E:\\Automation\\javaPractice\\java_practice\\JavaPractice\\src\\main\\resources\\Out\\"+filaName);
        this.fileHandler.setLine(writeList);
        this.fileHandler.writeToFile();
    }

    public String[] splitter(String regex){
        return readWords().split(regex);
    }

    public void writeFruitsWithVowels(){
        ArrayList<String> vowels = new ArrayList<>(Arrays.asList("A","a","E","e","I","i","O","o","U","u"));
        ArrayList<String> vowelList = new ArrayList<>();
        String[] word;

        try{

            word = (splitter("a fruit|like|, "));
            for(int i=0; i<word.length;i++){
                for (String vowel : vowels) {
                    if (word[i].startsWith(vowel)) {
                        vowelList.add(word[i]);
                    }
                }
            }
            writeWords(vowelList,"outFruits.txt");
        }catch (ArrayIndexOutOfBoundsException e){
            throw new ArrayIndexOutOfBoundsException("Array Out of bound");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void fruitCount(){
        String[] word = (splitter(", "));
        System.out.println(word.length);
    }
}
