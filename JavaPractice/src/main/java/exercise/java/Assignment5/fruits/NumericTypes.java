package exercise.java.Assignment5.fruits;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class NumericTypes extends FileHandler{
    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    private final FileHandler fileHandler;
    private final Map<String,Object[]> data;
    private final ArrayList<String> arrayList;
    private HSSFRow row;
    private HSSFCell cell;

    public NumericTypes() {
        this.workbook = new HSSFWorkbook();
        this.sheet = workbook.createSheet("Numeric Types");
        this.fileHandler = new FileHandler();
        this.data = new TreeMap<>();
        this.arrayList = new ArrayList<>(Arrays.asList("Byte","Short","Integer","Long","Float","Double"));
    }

    public void setWorkbook(String filePath) throws IOException {
        FileInputStream file = new FileInputStream(filePath);
        this.workbook = new HSSFWorkbook(file);
    }

    public void prepareExcel(){
        int i = 1;
        int k=0;
        int rowCount = -1;
        int cellnum;
        String type;

        data.put("0",new Object[]{"Data Types", "Max Value", "Min Value"});
        while (i<=6){
            type = arrayList.get(k);

            if (Objects.equals(type, "Byte")){
                data.put(Integer.toString(i),new Object[]{type, Byte.MAX_VALUE, Byte.MIN_VALUE});
            } else if (Objects.equals(type, "Short")) {
                data.put(Integer.toString(i),new Object[]{type, Short.MAX_VALUE, Short.MIN_VALUE});
            }else if (Objects.equals(type, "Integer")) {
                data.put(Integer.toString(i),new Object[]{type, Integer.MAX_VALUE, Integer.MIN_VALUE});
            }else if (Objects.equals(type, "Long")) {
                data.put(Integer.toString(i),new Object[]{type, Long.MAX_VALUE, Long.MIN_VALUE});
            }else if (Objects.equals(type, "Float")) {
                data.put(Integer.toString(i),new Object[]{type, Float.MAX_VALUE, Float.MIN_VALUE});
            }else if (Objects.equals(type, "Double")) {
                data.put(Integer.toString(i),new Object[]{type, Double.MAX_VALUE, Double.MIN_VALUE});
            }
            i++;
            k++;
        }

        Set<String> keyset = data.keySet();

        for (String key: keyset) {
            this.row = sheet.createRow(++rowCount);
            Object[] objArr = data.get(key);
            cellnum =0;
            for (Object obj : objArr) {
                this.cell = this.row.createCell(cellnum++);
                if (obj instanceof String)
                    this.cell.setCellValue((String)obj);
                else if (obj instanceof Integer)
                    this.cell.setCellValue((Integer)obj);
                else if (obj instanceof Byte)
                    this.cell.setCellValue((Byte)obj);
                else if (obj instanceof Short)
                    this.cell.setCellValue((Short)obj);
                else if (obj instanceof Long)
                    this.cell.setCellValue((Long)obj);
                else if (obj instanceof Float)
                    this.cell.setCellValue((Float)obj);
                else if (obj instanceof Double)
                    this.cell.setCellValue((Double) obj);
            }

        }
    }

    @Override
    public void writeToFile() throws IOException {
        fileHandler.setFilePath("E:\\Automation\\javaPractice\\java_practice\\JavaPractice\\src\\main\\resources\\Out\\NumericTypes.xls");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileHandler.getFilePath());
            workbook.write(fileOutputStream);
            fileOutputStream.close();
        }catch (IOException e){
            throw new IOException("IOException!",e);
        }
    }

    public void displayInConsole() throws IOException {
        int i =0;
        this.setWorkbook(fileHandler.getFilePath());
        this.workbook.getSheetAt(0);
        Iterator<Sheet> rowIterator = this.workbook.iterator();
        try{
            while (rowIterator.hasNext()){
            this.row = this.workbook.getSheetAt(0).getRow(i);
            if(this.row == null){
                break;
            }
            else{
            Iterator<Cell> cellIterator = this.row.cellIterator();

            while (cellIterator.hasNext())
            {
                HSSFCell cell = (HSSFCell) cellIterator.next();
                //Check the cell type and format accordingly
                switch (cell.getCellType()) {
                    case NUMERIC -> System.out.printf("%-25s\t", cell.getNumericCellValue());
                    case STRING -> System.out.printf("%-25s\t",cell.getStringCellValue());
                    default -> throw new IllegalStateException("Unexpected value: " + cell.getCellType());
                }
            }
            System.out.println();
            i++;}
        }
        }catch (NullPointerException e){
            throw new NullPointerException("Null Point Exception."+e);
        }

    }

    public void shiftCellstoRight(int rowNumber){
        this.sheet = this.workbook.getSheetAt(0);
        this.row = this.sheet.getRow(rowNumber);
        this.row.shiftCellsRight(0,3,1);
    }
    public void addMoreColumn(int colLocation, String columnName) throws IOException {
        int i=0;
        int k=0;
        this.setWorkbook(fileHandler.getFilePath());
        HashMap<String, String> link = new HashMap<>(){{
            put("google","https://www.google.com/");
            put("facebook","https://web.facebook.com/");
            put("youtube","https://youtube.com/");
        }};

        while(i<=this.sheet.getLastRowNum()){
            shiftCellstoRight(i);
            i++;
        }
        this.row = this.sheet.getRow(k);
        this.row.createCell(colLocation).setCellValue(columnName);
        k++;
        for(String key:link.keySet()){
            this.row = this.sheet.getRow(k);
            this.cell=this.row.createCell(colLocation);
            Object[] objArr = new String[]{link.get(key)};
            for (Object obj : objArr) {
                this.cell.setCellFormula("HYPERLINK(\"" + obj.toString() + "\", \"" + key + "\")");
            }
            k++;
        }
        writeToFile();
        System.out.println("Data added to the file.");
    }
}
