package exercise.java.Assignment5.fruits;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        Fruits fruits = new Fruits();
        fruits.writeFruitsWithVowels();
        fruits.fruitCount();

        NumericTypes numericTypes = new NumericTypes();
        numericTypes.prepareExcel();
        numericTypes.writeToFile();
        numericTypes.displayInConsole();
        numericTypes.addMoreColumn(0, "Web Sites");
    }
}