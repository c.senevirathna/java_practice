package exercise.java.Assignment5.fruits;

import java.io.*;
import java.util.ArrayList;

public class FileHandler {

    String filePath;
    ArrayList<String> line;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setLine(ArrayList<String> line) {
        this.line = line;
    }

    public String readFile() {

        try {

            BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
            String line;

            if((line = reader.readLine()) == null){
                System.out.println("Empty Document!!");
            }
            reader.close();
            return line;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to find the file", e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToFile() throws IOException {

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.filePath));

            for (int i=0; i<this.line.size(); i++){
                writer.write(String.valueOf(this.line.get(i))+"\n");
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    }
