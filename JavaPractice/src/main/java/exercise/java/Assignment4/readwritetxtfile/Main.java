package exercise.java.Assignment4.readwritetxtfile;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        FileHandler fileHandler = new FileHandler();
        fileHandler.setFilePath("E:\\Automation\\javaPractice\\java_practice\\JavaPractice\\src\\main\\resources\\");
        fileHandler.readWriteFile();
        System.out.println("No. of Lines: "+ fileHandler.getNoOfLines());
        fileHandler.writeToFile();
    }
}
