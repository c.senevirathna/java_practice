package exercise.java.Assignment4.readwritetxtfile;

import java.io.*;

public class FileHandler {

    String filePath;
    int noOfLines;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getNoOfLines() {
        return noOfLines;
    }

    public void setNoOfLines(int noOfLines) {
        this.noOfLines = noOfLines;
    }

    public void readWriteFile(){

        try {

            BufferedReader reader = new BufferedReader(new FileReader(this.filePath+"TestData\\readme.txt"));
            String line;
            int count=0;

            while ((line= reader.readLine())!=null){
                System.out.println(line);
                count++;
            }
            setNoOfLines(count);
            reader.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException("Unable to find the file",e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToFile() throws IOException {

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.filePath+"Out\\outFile.txt"));
            String line = "writting a message";

            writer.write(line);
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
