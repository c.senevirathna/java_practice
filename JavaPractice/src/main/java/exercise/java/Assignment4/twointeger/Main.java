package exercise.java.Assignment4.twointeger;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<Integer> intList = new ArrayList<>(2);
        IntergerDevider intergerDevider = new IntergerDevider();

        while (intList.size() < 2){
            System.out.println("Enter an integer: ");
            try{
                intList.add(sc.nextInt());
            }
            catch (InputMismatchException e){
                throw new InputMismatchException("Entered value was not an innteger.");
            }
        }
        intergerDevider.devider(intList);
        System.out.println("Numbers devided. Result: "+ intergerDevider.getResult());
    }
}
