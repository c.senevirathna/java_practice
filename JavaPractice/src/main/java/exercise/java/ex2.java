// check odd or even numbers
package exercise.java;

import java.util.Scanner;

public class ex2 {

    public void oddOrEven(){
        Scanner sc = new Scanner(System.in);
        int value = Integer.parseInt(sc.nextLine());
        if(value % 2 == 1)
            System.out.println(value+" is an odd number");
        else
            System.out.println(value+" is an even number");
    }
}
