package exercise.java;

public class StartPattern {
    public void printStars(){

        for(int k=1; k<8; k++) {
            if(k%2==0){
                 continue;
            }
            else{

            for (int i=k; i<=k; i--) {
                if(i<1){
                    break;
                } else
                {
                    System.out.print("*");
                }

            }
                System.out.println(" ");
            }
        }

        for(int k=5; k>0; k--) {
            if(k%2==0){
                continue;
            }
            else{
                for (int i=k; i<=k; i--) {
                    if(i<1){
                        break;
                    } else
                    {
                        System.out.print("*");
                    }

                }
                System.out.println(" ");
            }
        }
    }}
