package exercise.java.payrollsystem;

public class BasePlusCommissionEmployee extends CommissionEmployee implements Invoice{
    private double baseSalary;
    public BasePlusCommissionEmployee(String firstName, String lastName, int idNumber, double commissionRate, int grossSales, double baseSalary) {
        super(firstName, lastName, idNumber, commissionRate, grossSales);
        this.baseSalary = baseSalary;
    }

    public double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    @Override
    protected  double getEarnings(){
        return this.getCommissionRate() * this.getGrossSales() + this.getBaseSalary();
    }

    @Override
    public String toString() {
        return "Invoice" + '\n' +
                "Employee Number:" + super.getIdNumber() + '\n' +
                "Employee Name:" + super.getFirstName() + " " + super.getLastName() + '\n' +
                "Employee Type:" + "Base Plus Commission Employee" + '\n' +
                "Base Salary:" + this.getBaseSalary() + '\n' +
                "Salary:" + this.getEarnings() + '\n' + '\n';
    }

    @Override
    public void displayInvoice(){
        System.out.println(this.toString());
    }
}
