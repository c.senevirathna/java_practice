package exercise.java.payrollsystem;

public class Main {
    public static void main(String[] args) {

        Employee salariedEmployee = new SalariedEmployee("Daniel", "Sharov", 12056);
        salariedEmployee.displayInvoice();

        Employee hourlyEmployee = new HourlyEmployee("Tatyana", "Revinel", 12057, 1200, 35.25);
        hourlyEmployee.displayInvoice();

        Employee commissionEmployee = new CommissionEmployee("Nauran", "Kreshya",12058, 10,52000);
        commissionEmployee.displayInvoice();

        Employee basePlusCommissionEmployee = new BasePlusCommissionEmployee("sharif", "Kelly",12059, 10, 52622, 45280.25);
        basePlusCommissionEmployee.displayInvoice();
    }
}