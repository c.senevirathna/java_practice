package exercise.java.payrollsystem;

public abstract class Employee {
    private String firstName;
    private String lastName;
    private int idNumber;

    public Employee(String firstName, String lastName, int idNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }
    protected abstract double getEarnings();

    @Override
    public String toString() {
        return "Invoice" + '\n' +
                "Employee Number:" + this.getIdNumber() + '\n' +
                "Employee Name:" + this.getFirstName() + " " + this.getLastName() + '\n';
    }

    public abstract void displayInvoice();
}
