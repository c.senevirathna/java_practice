package exercise.java.payrollsystem;

public class HourlyEmployee extends Employee{
    private double wage;
    private double hours;

    public HourlyEmployee(String firstName, String lastName, int idNumber, double wage, double hours) {
        super(firstName, lastName, idNumber);
        this.hours = hours;
        this.wage = wage;
    }

    @Override
    protected double getEarnings() {
        double earning;

        if(this.hours <=40){
            earning=this.hours * this.wage;
        }
        else{
            earning = (40 * this.wage) + ((this.hours-40)* this.wage * 1.5);
        }
        this.setWage(earning);
        return 0;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "Invoice" + '\n' +
                "Employee Number:" + super.getIdNumber() + '\n' +
                "Employee Name:" + super.getFirstName() + " " + super.getLastName() + '\n' +
                "Employee Type:" + "Hourly Employee" + '\n' +
                "Worked Hours:" + this.getHours() + '\n' +
                "Salary:" + this.getWage() + '\n' + '\n';
    }

    @Override
    public void displayInvoice() {
        System.out.println(this.toString());
    }
}
