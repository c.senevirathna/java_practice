package exercise.java.payrollsystem;

public class CommissionEmployee extends Employee{
    private double commissionRate;
    private int grossSales;
    public CommissionEmployee(String firstName, String lastName, int idNumber, double commissionRate, int grossSales) {
        super(firstName, lastName, idNumber);
        this.commissionRate = commissionRate;
        this.grossSales = grossSales;
    }

    @Override
    protected double getEarnings() {
        return this.getCommissionRate() * this.getGrossSales();
    }

    public double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(double commissionRate) {
        this.commissionRate = commissionRate;
    }

    public int getGrossSales() {
        return grossSales;
    }

    public void setGrossSales(int grossSales) {
        this.grossSales = grossSales;
    }

    @Override
    public String toString() {
        return  "Invoice" + '\n' +
                "Employee Number:" + super.getIdNumber() + '\n' +
                "Employee Name:" + super.getFirstName() + " " + super.getLastName() + '\n' +
                "Employee Type:" + "Commission Employee" + '\n' +
                "Commission Rate:" + this.getCommissionRate() + '\n' +
                "Salary:" + this.getEarnings() + '\n' + '\n';
    }

    @Override
    public void displayInvoice() {
        System.out.println(this.toString());}
}
