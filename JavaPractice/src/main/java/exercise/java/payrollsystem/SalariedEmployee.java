package exercise.java.payrollsystem;

public class SalariedEmployee extends Employee implements Invoice{
    private double weeklySalary;
    public SalariedEmployee(String firstName, String lastName, int idNumber) {
        super(firstName, lastName, idNumber);
    }

    @Override
    protected double getEarnings() {
        this.setWeeklySalary(25475.32);
        return 0;
    }

    public double getWeeklySalary() {
        this.getEarnings();
        return this.weeklySalary;
    }

    public void setWeeklySalary(double weeklySalary) {
        this.weeklySalary = weeklySalary;
    }

    @Override
    public String toString() {
        return "Invoice" + '\n' +
                "Employee Number:" + super.getIdNumber() + '\n' +
                "Employee Name:" + super.getFirstName() + " " + super.getLastName() + '\n' +
                "Employee Type:" + "Salaried Employee" + '\n' +
                "Salary:" + this.getWeeklySalary() + '\n' + '\n';
    }

    @Override
    public void displayInvoice() {
        System.out.println(this.toString());
    }
}
