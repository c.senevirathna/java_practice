package exercise.java.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Calculation implements Calculator{
    double number = 0;
    double previousNum = 0;
    String arithmeticOperation = "";

    public void setNumber(Double number) {
        this.number = number;
    }

    public void setPreviousNum(double previousNum) {
        this.previousNum = previousNum;
    }

    public double getPreviousNum() {
        return previousNum;
    }

    public void printVals(double num1, String operator, double num2, double total) {
        System.out.println(num1 + operator + num2 + " = " + total);
    }

    public void setArithmeticOperation(String arithmeticOperation) {
        this.arithmeticOperation = arithmeticOperation;
    }

    public void generateTot(){
        double total =0;
        try {
            if(arithmeticOperation.equals("+")){
                total = previousNum + number;
            } else if (arithmeticOperation.equals("-")) {
                total = previousNum - number;
            } else if (arithmeticOperation.equals("*")) {
                total = previousNum * number;
            }else{
                total = previousNum / number;
            }
            printVals(previousNum, arithmeticOperation, number, total);
            this.setPreviousNum(total);
        }catch (Exception e){
            System.out.println(e);
        }

    }

    @Override
    public double calculate(ArrayList<String> inputVal) {
        Set<String> operators = new HashSet<>(Arrays.asList("+", "-", "*", "/"));
        for(int i = 0; i<inputVal.size(); i++){
            try{

                if(i == 0){
                    this.setPreviousNum(Double.parseDouble(inputVal.get(i)));
                }
                else if (operators.contains(inputVal.get(i))){
                    this.setArithmeticOperation(inputVal.get(i));
                }
                else{
                    this.setNumber(Double.parseDouble(inputVal.get(i)));
                    generateTot();
                }
            }
            catch (Exception e){
                System.out.println(e);
            }
        }
        return getPreviousNum();
    }
}
