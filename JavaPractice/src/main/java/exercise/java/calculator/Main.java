package exercise.java.calculator;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String inputValue = null;
        double previousVal = 0.0;
        ArrayList<String> list = new ArrayList<>(3);
        Scanner sc = new Scanner(System.in);
        Calculation calc = new Calculation();

        while (inputValue != "=") {
            while (list.size() < 3) {
                System.out.println("Enter number or an operation:");
                inputValue = sc.next();
                if(inputValue.equals("=")){break;}
                list.add(inputValue);
            }
            if(inputValue.equals("=")){break;}
            previousVal = calc.calculate(list);
            list.clear();
            list.add(0, String.valueOf(previousVal));
        }
    }
}
