package exercise.java.calculator;

import java.util.ArrayList;

public interface Calculator {

    double calculate(ArrayList<String> inputVal);
}
