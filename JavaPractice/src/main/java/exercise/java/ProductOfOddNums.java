package exercise.java;

import java.time.Instant;
import java.time.temporal.ValueRange;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.EnumSet.range;


public class ProductOfOddNums {
    public void findProduct(){
        int product = 1;
        IntStream intStream = IntStream.range(1,16);
        int[] array = intStream.toArray();

        for(int i = 0; i<array.length; i++){
            if(array[i]%2!=0){
                product = product * array[i];
                System.out.println(array[i]);
            }
        }
        System.out.println("Product of range(1,15): "+ product);

    }
}
