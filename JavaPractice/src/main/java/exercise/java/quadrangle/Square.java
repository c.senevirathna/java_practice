package exercise.java.quadrangle;

public class Square extends Quadrangle{

    protected double getArea(int x) {
        return x*x;
    }

    protected double getPerimeter(int x) {
        return 4*x;
    }

    @Override
    protected double getArea(int x, int y) {
        return 0;
    }

    @Override
    protected double getPerimeter(int x, int y) {
        return 0;
    }
}
