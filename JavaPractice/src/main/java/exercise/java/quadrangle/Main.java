package exercise.java.quadrangle;

public class Main {
    public static void main(String[] args) {
        Quadrangle rectangle = new Rectangle();
        Square sqaure = new Square();
        System.out.println("Area of the rectangle: "+ rectangle.getArea(12, 5));
        System.out.println("Perimeter of the rectangle: "+ rectangle.getPerimeter(12,5));
        System.out.println("Area of the square: "+ sqaure.getArea(4));
        System.out.println("Perimeter of the square: "+ sqaure.getPerimeter(4));
    }
}
