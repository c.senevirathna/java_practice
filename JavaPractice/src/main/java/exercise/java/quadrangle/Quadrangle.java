package exercise.java.quadrangle;

public abstract class Quadrangle {

    protected abstract double getArea(int x, int y);
    protected abstract double getPerimeter(int x, int y);

}
