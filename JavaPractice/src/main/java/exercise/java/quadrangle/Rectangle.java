package exercise.java.quadrangle;

public class Rectangle extends Quadrangle{
    @Override
    protected double getArea(int x, int y) {

        return x * y;
    }

    @Override
    protected double getPerimeter(int x, int y) {
        return 2*(x + y);
    }
}
